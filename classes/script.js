
class Rectangle {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
    calcArea() {
        return this.height * this.width;
    }
}

class ColoredRectangleWithText extends Rectangle {
    constructor(height, width, text, bgColor) {
        super(height, width); //вызывает супер констркутор родителя, чтоб кадый раз не перечислять свойства родительского класса (всегда пишем в начале констркутора)
        this.text = text;
        this.bgColor = bgColor;
    }

    showMyProps() {
        console.log(`Текст ${this.text} цвет ${this.bgColor}`);
    }
}

// const square = new Rectangle(10, 10);
// const long = new Rectangle(20, 100);

// console.log(square);
// console.log(square.calcArea());
// console.log(long.calcArea());

const div = new ColoredRectangleWithText(25, 10, 'hello', 'red');

div.showMyProps();
console.log(div.calcArea());

//Абстаркция - когда отделяем концепцию от её экземпляра
//Наслдеоваие - способность оюъекта или класса базироваться на другом объекте или классе 

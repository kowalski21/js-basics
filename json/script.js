const person = {
    name: 'Alex',
    tel: '79111111111',
    parents: {
        mom: 'Olga',
        dad: 'Mike'
    }
}

//глубокое клонирование объекта
const clone = JSON.parse(JSON.stringify(person))
clone.parents.mom = 'Ann';
console.log(clone);
console.log(person);



console.log(JSON.parse(JSON.stringify(person)));

const someJSON = '{"name":"Alex","tel":"7911111111"}'
console.log(JSON.parse(someJSON));
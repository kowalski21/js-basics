'use strict';

//filter - возвращаент новый массив в отличии от forEach

const names = ['ivan', 'ann', 'ksenia', 'voldemart'];

const shortNames = names.filter((name) => {
    return name.length < 5;
});

console.log(shortNames);

//map - возвращает новый массив (берем исходный)

const answers = ['IvAn', 'AnnA', 'Hello'];

const result = answers.map((item) => {
    return item.toLowerCase();
})

console.log(result);
console.log(answers); //исходный массив не изменен

//every/some - используем когда хотим узнать когда хотим проверить все
//елементы или когда хотим проверить подходит хотяб один элемент
//таким образом избавляемся от forEach

//some - берет массив перебирает его и если хотябы один элемент подходит под
//условие в колбеке то метод нам вернет true, если нет то false

//every - ерет массив перебирает его и если все элементы подходят под услотвие
//вернет true

const arr = [4, 'qwq', 'dasdasd'];
const arr1 = [4, 'qwq', 'dasdasd'];

// console.log(arr.some(item => {
//     return typeof (item) === 'number';
// }));

console.log(arr1.every(item => {
    return typeof (item) === 'number';
}));

//reduce - схлопывает или собирает массив в единое целое

const arrReduce = [4, 5, 1, 3, 2, 6];

const resultReduce = arrReduce.reduce((acc, currentValue) => {
    return acc + currentValue;
}, 0); // 0 - initial value of acc

console.log(resultReduce);

const arrReduceString = ['apple', 'pear', 'plum'];

const resultReduceString = arrReduceString.reduce((acc, currentValue) => {
    return `${acc}, ${currentValue}`;
}, 'eba'); //если не указаывать начальное значение acc,
//то начальным значением для acc будет первое значение из массива

console.log(resultReduceString); //string

const obj = {
    ivan: 'persone',
    ann: 'persone',
    dog: 'animal',
    cat: 'animal'
};
//Object.entries - превратит объект в массив массивов

//chaining
const newArr = Object.entries(obj)
    .filter((item) => {
        return item[1] === 'persone';
    })
    .map((item) => {
       return item[0];
    });

console.log(newArr);

const films = [
    {
        name: 'Titanic',
        rating: 9
    },
    {
        name: 'Die hard 5',
        rating: 5
    },
    {
        name: 'Matrix',
        rating: 8
    },
    {
        name: 'Some bad film',
        rating: 4
    }
];

function showGoodFilms(arr) {
    return arr.filter((item) => {
        return item.rating >= 8;
    }).map((item) => {
        return item;
    });
}
console.log(showGoodFilms(films));

function showListOfFilms(arr) {
    return arr.map(item => {
        return item.name;
    }).reduce((acc, curr) => {
        return `${acc}, ${curr}`
    });
}
console.log(showListOfFilms(films));

function setFilmsIds(arr) {
    return arr.map((item, index) => {
        item.id = index;
        return item;
    });
}

console.log(setFilmsIds(films));


const transformedArray = setFilmsIds(films);

function checkFilms(arr) {
    return arr.every(item => {
        return item.id || item.id === 0
    })
}
console.log(checkFilms(transformedArray));

console.log('---------------------------------')

const funds = [
    {amount: -1400},
    {amount: 2400},
    {amount: -1000},
    {amount: 500},
    {amount: 10400},
    {amount: -11400}
];

const getPositiveIncomeAmount = (data) => {
    return data.filter(item => {
        return item.amount > 0;
    }).map(item => {
        return item.amount;
    }).reduce((acc, curr) => {
        return acc + curr;
    })
};

console.log(getPositiveIncomeAmount(funds));
const getTotalIncomeAmount = (data) => {
    if (data.some(item => {
        return item.amount < 0
    })) {
        return data.map(item => {
            return item.amount;
        })
            .reduce((acc, curr) => {
                return acc + curr;
            })
    } else {
        getPositiveIncomeAmount(data);
    }
};

console.log(getTotalIncomeAmount(funds));

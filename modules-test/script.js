
const number = 1;

//нативная реализация модуля
(function(){
    let number = 2;
    console.log(number);
    console.log(number + 3);
}());

console.log(number);


//использование объектного интерфейса
const user = (function() {
    //нет доступа к этой фу-ци так как она в локальной обл видимости
    const privat = function() {
        console.log('I am privat');
    };

    return {
        sayHello: privat
    };
}());

user.sayHello();
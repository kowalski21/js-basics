'use strict';

const box = document.querySelector('.box');
const block = document.querySelector('.block');

//console.log(block)

//console.log(block?.textContent); //ошибка

//console.log(1 + 2);

//оператор опциональной цеочки, проверяет выражение слева от себя

const userData = {
    name: 'Ivan',
    age: null,
    say: function () {
        console.log('Hello');
    }
}

userData.say();
userData.hey?.(); //проверит а есть ли такой метод, если нет вернет undefined

//console.log(userData?.skills?.js);

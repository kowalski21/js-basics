'use strict';

const data = [
    {
        id: 'box',
        tag: 'div'
    },
    {
        id: 'ff',
        tag: 'nav'
    },
    {
        id: 'circle',
        tag: ''
    }
]

try {
    data.forEach((blockObj, i) => {
        const block = document.createElement(blockObj.tag);
    
        if (!blockObj.id) throw new SyntaxError(`В данных под номер ${i + 1} нет id`);
        block.setAttribute('id', blockObj.id);
        document.body.append(block);
    });
} catch(e) {
    if (e.name === "SyntaxError") {
        console.log(e.message);
    } else throw e; //тут мы прокидываем ошибку дальше чтоб её точно кто-то увидел
   
}

// const err = new SyntaxError('dasd');
// console.log(err.name);
// console.log(err.message);
// console.log(err.stack);
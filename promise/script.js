'use strict';

// console.log('Запрос данных');

// const req = new Promise(function(resolve, reject) {
//     setTimeout(()=>{
//         console.log('Подготовка данных');
    
//         const product = {
//             name: 'TV',
//             price: '2000'
//         };
    
//         resolve(product);
//     }, 2000);

    
// });

// req.then((product) => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             product.status = 'order';
//             //reject();
//             resolve(product);
//         }, 2000);
//     });
//    }).then(data => {
//     data.modify = true;
//     return data;
    
//    }).then((data) => {
//     console.log(data);
//    }).catch(() => {
//     console.error('Произошла ошибка');
//    }).finally(()=>{
//     console.log('Finally');
//    });
    

const test = time => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve();
        }, time)
    })
}

// test(1000).then(()=>console.log('1000 ms'));
// test(2000).then(()=>console.log('2000 ms'));

// Promise.all([test(1000), test(2000)]).then(() => {
//     console.log('Все промисы выполнены all')
// });

Promise.race([test(1000), test(2000)]).then(() => {
    console.log('race')
});

const myPromise = new Promise((resolve, reject) => {
    //resolve('hello world');
    reject('some error');
});

myPromise.then(value => {
    console.log(value);
}).catch(error => {
    console.log(error);
});

fetch('https://jsonplaceholder.typicode.com/todos/1') //возращает промис в сотоянии pending
    .then(response => {
        return response.json()
            .then(data => {
                console.log(data);
            });
    });

//функция котороя упростит взаимодействие с fetch (один промис)
const getData = (url) => {
    return new Promise((resolve, reject) => {
        fetch(url)
            .then(response => response.json())
            .then(data => resolve(data))
            .catch(error => reject(error.message))
    });
}

getData('https://jsonplaceholder.typicode.com/todos/10')
    .then(data => console.log(data))
    .catch(error => console.log(error));

const getGitHubUserJobs = async (names) => {
    let jobs = [];
    for (name of names) {
        let job = fetch(`https://api.github.com/users/${name}`)
            .then(
                successResponse => {
                    if (successResponse.status !== 200) {
                        return null;
                    } else {
                        return successResponse.json();
                    }
                },
                failResponse => {
                    return null;
                }
            );
        jobs.push(job);

    }

    let results = await Promise.all(jobs);
    return results;
}

getGitHubUserJobs(['kowalski21']).then(data => console.log(data));



const p1 = new Promise((resolve, reject) => {
    setTimeout(resolve, 1000, "one");
});
const p2 = new Promise((resolve, reject) => {
    setTimeout(resolve, 2000, "two");
});
const p3 = new Promise((resolve, reject) => {
    setTimeout(resolve, 3000, "three");
});
const p4 = new Promise((resolve, reject) => {
    setTimeout(resolve, 4000, "four");
});
const p5 = new Promise((resolve, reject) => {
    setTimeout(reject, 4000, "reject!");
});

Promise.race([p1, p2, p3, p4, p5]).then(value => {
    console.log(value);
}, reason => {
    console.log(reason)
});















'use strict'

const inputRub = document.querySelector('#rub'),
      inputUsd = document.querySelector('#usd');

inputRub.addEventListener('input', () => {
    const request = new XMLHttpRequest();

    //request.open(method, url, async, login, pass);
    request.open('GET', 'js/current.json');
    request.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    request.send();

    // request.addEventListener('readystatechange', () => { //readystatechange - отслеживает статус готовности нашего запроса в данный текущий момент (следит за readyState)
    //     if (request.readyState === 4 && request.status === 200) {
    //         console.log(request.response);
    //         const data = JSON.parse(request.response);
    //         inputUsd.value = (+inputRub.value / data.current.usd).toFixed(2);
    //     } else {
    //         inputUsd.value = "что то не так";
    //     }
    // });

    request.addEventListener('load', () => { //load - срабатывает только один раз когда запрос полностью готов, но нам важен статус код
        if (request.status === 200) {
            console.log(request.response);
            const data = JSON.parse(request.response);
            inputUsd.value = (+inputRub.value / data.current.usd).toFixed(2);
        } else {
            inputUsd.value = "что то не так";
        }
    });

    //свойства XMLHttpRequest
    //status
    //statusText
    //response
    //readyState - текущее состояние нашего запроса


});
const person = {
    name: "alex",
    age: 25,

    get userAge() {
        return this.age;
    },

    set userAge(num) {
        this.age = num;
    }
}

console.log(person.userAge = 30);
console.log(person.userAge);

//Инкапсуляция
function User(name, age) {
    this.name = name;
    let userAge = age;

    this.say = function() {
        console.log(`Имя пользователя ${this.name} возрат ${userAge}`);
    };

    this.getAge = function() {
        return userAge;
    };

    this.setAge = function(age) {
        if (typeof age === 'number' && age > 0 && age < 110) {
            userAge = age;
        } else {
            console.log('Недопустимое значение');
        }
    }
}

const ivan = new User('Ivan', 27);
console.log(ivan.name);
//console.log(ivan.userAge);
console.log(ivan.getAge());


//ivan.age = 30;
ivan.setAge(30);
ivan.setAge(300);
console.log(ivan.getAge());  

ivan.say();





class User1 {
    constructor(name, age) {
        this.name = name;
        this._userAge = age;
    }
    
    #surname = 'Black';

    say = () => {
        console.log(`Имя пользователя ${this.name} ${this.#surname} возрат ${this._userAge}`);
    }

    get userAge() {
        return this._userAge;
    }

    set userAge(age) {
        if (typeof age === 'number' && age > 0 && age < 110) {
            this._userAge = age;
        } else {
            console.log('Недопустимое значение');
        }
    }

    get surname() {
        return this.#surname;
    }

    set surname(sname) {
        this.#surname = sname;
    }
}

const ivan1 = new User1('Ivan', 27);
//console.log(ivan1.surname);
//ivan1.surname('new');
//console.log(ivan1.surname);
console.log(ivan1.userAge);
ivan1.userAge = 120;
console.log(ivan1.userAge);

// console.log(ivan1.name);
// //console.log(ivan.userAge);
// console.log(ivan1.getAge());


// //ivan.age = 30;
// ivan1.setAge(30);
// ivan1.setAge(300);
// console.log(ivan1.getAge());  

ivan1.say();


class CoffeeMachine {

    #waterAmount = 0;
  
    get waterAmount() {
      return this.#waterAmount;
    }
  
    set waterAmount(value) {
      if (value < 0) throw new Error("Отрицательный уровень воды");
      this.#waterAmount = value;
    }
  }
  
  let machine = new CoffeeMachine();
  
//   machine.waterAmount = 0;
  console.log(machine.waterAmount);
  machine.waterAmount = -10;
  console.log(machine.waterAmount);
  //alert(machine.#waterAmount); // Error
// флаги в объектах
//writable если true то свойство в объектах можно будет изменить, если false то только для чтения
// enumerable если true то свойство будет перечисляться в циклах
// configurable если true то свойство можно будет удалить а атрибуты изменить

const user = {
    name: 'Alex',
    surname: 'Smith',
    [Symbol('birthday')]: '20/04/2021',
    //birthday: '20/04/1993',
    showMyPublicData: function() {
        console.log(`${this.name} ${this.surname}`);
    }
}
for (let key in user) console.log(key); //игнорирует символы



Object.defineProperties(user, {
    name: {writable: false},
    surname: {writable: false}
});

console.log(Object.getOwnPropertyDescriptor(Math, 'PI'));

Object.defineProperty(user, 'showMyPublicData', {enumerable: false});
for (let key in user) console.log(key);


Object.defineProperty(user, 'birthday', {value: prompt('Date?'), enumerable: true, configurable: true});


Object.defineProperty(user, 'birthday', {writable: false});
console.log(Object.getOwnPropertyDescriptor(user, 'birthday'));
// теперь не можем поменять birthday
// user.showMyPublicData();
console.log(Object.getOwnPropertyDescriptor(user, 'name'));

Object.defineProperty(user, 'name', {writable: false});
Object.defineProperty(user, 'gender', {value: 'male'});
console.log(Object.getOwnPropertyDescriptor(user, 'gender')); //все флаги стоят в false
//user.name = 'dasd';


 


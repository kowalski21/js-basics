'use strict';
function showThis(a, b) {
    console.log(this); //Window ссылается на глобальный объект
    function sum() {
        console.log(this);
        return a + b;
    }

    console.log(sum());
} 

showThis(4, 5);

const obj = {
    a: 20,
    b: 15,
    sum: function () {
        //console.log(this);
        function shout() {
            console.log(this); //undefiend
        }
        shout();
    }
}

obj.sum();


function User (name, id) { //фун-ия конструктор когда она будет вызывана она создаст новый объект
    this.name = name;
    this.id = id;
    this.human = true;
    this.hello = function() {
        console.log("Hello! " + this.name);
    }
}

let ivan = new User('Ivan', 23); //ivan - это экземпляр объекта
 

function sayName(surname) {
    console.log(this);
    console.log(this.name + surname);
}

const user = {
    name: 'John'
};

sayName.call(user, ' Smith');
sayName.apply(user, [' Black']);


function count(num) {
    return this*num;
}

const double = count.bind(2);
console.log(double(3));
console.log(double(13));


// 1) Обынчая функция this = window, но если use strict - То будет undefined
// 2) Контекст у методов объекта - это будет сам объект
// 3) this в констркуторах и классах - это новый экземпляр объекта
// 4) Ручная привязка this: call, apply, bind

//anonymous func
const btn = document.querySelector('button');
btn.addEventListener('click', function() {
    this.style.background = 'red';
    console.log(this); //будет как e.target
});

//arrow func нет своего контекста вызова берет у родителя контекст
const obj1 = {
    num: 5,
    sayNumer: function() {
        const say = () => {
            console.log(this.num);
        }

        say();
    }
}

obj1.sayNumer();

const double1 = a => a * 2;
